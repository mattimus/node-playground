(function (seedData) {
    seedData.initialNotes = [{
            name: "History",
            notes: [{
                    note: "Testing history",
                    author: "Shawn",
                    color: "yellow"
                }, {
                    notes: "I like history",
                    author: "Shawn",
                    color: "blue"
                }, {
                    note: "I hate war",
                    author: "Shawn",
                    color: "green"
                }, {
                    note: "History is an interesting subject.",
                    author: "Shawn",
                    color: "green"
                }, {
                    note: "Presidents are cool.",
                    author: "Shawn",
                    color: "orange"
                }]
        }, {
            name: "People",
            notes: [{
                    note: "Jefferson was a President.",
                    author: "Shawn",
                    color: "yellow"
                }, {
                    note: "John Wayne was an actor",
                    author: "Shawn",
                    color: "blue"
                }, {
                    note: "Reagan was a President and an actor.",
                    author: "Shawn",
                    color: "green"
                }]
        }];
})(module.exports);
