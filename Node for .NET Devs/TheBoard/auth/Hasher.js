﻿(function (hasher) {
    var crypto = require("crypto");
    
    hasher.createSalt = function () {
        var len = 8;
        return crypto.randomBytes(Math.ceil(len / 2)).toString('hex').substring(0, len);
    };
    
    hasher.hash = function (password, salt) {
        // sha 1 sucks.
        var hmac = crypto.createHmac("sha512", salt);
        var hash = hmac.update(password).digest("hex");
        return hash;
    };
})(module.exports);