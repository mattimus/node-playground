﻿ // http library allows you to listen for http requests and respond to them
var http = require("http");
var express = require("express");
var bodyParser = require('body-parser');
var flash = require("connect-flash");
var cookieParser = require("cookie-parser");
var session = require("express-session");

var app = express();

var controllers = require('./controllers');

// Setup View Engine
// make vash templates get interpretted as html in VS http://upnxt.com/blog/node-with-express-vash-sqlite
app.set("view engine", "vash");

// Order of app.use MATTERS!!!
// set the public static resources folder
app.use(express.static(__dirname + "/public"));

// opt into services
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json()); // need to be able to send in json post data
app.use(cookieParser()); // needed to be able to use flash
app.use(session({
    // needed to be able to use flash
    secret: "TheBoard",
    resave: false,
    saveUninitialized: true
}));
app.use(flash()); // needed to be able to use flash

// use authentication, start up the auth before setting any routes up
var auth = require("./auth");
auth.init(app);

// setup routing
controllers.init(app);

app.get("/api/users", function (req, res) {
    res.set("Content-Type", "application/json");
    res.send({
        name: "Shawn",
        isValid: true,
        group: "Admin"
    });
});

// create server, then make the server listen for requests
var server = http.createServer(app);
server.listen(3000);

// setup socket.io to listen/respond to requests
var updater = require("./updater");
updater.init(server);