﻿(function (angular) {
    var theModule = angular.module("notesView", ["ui.bootstrap"]); // empty array means no dependencies
    theModule.controller("notesViewController", 
        // the reason for using "$scope" then $scope is so that this will still work after minification. 
        // $scope will probably be changed to a single character after minification.
        // the function ($scope) is our controller
        ["$scope", "$window", "$http",
            function ($scope, $window, $http) {
            $scope.notes = [];
            $scope.newNote = createBlankNote();
            
            // get the category name
            var urlParts = $window.location.pathname.split("/");
            var categoryName = urlParts[urlParts.length - 1];
            
            var notesApiUrl = "/api/notes/" + categoryName;
            $http.get(notesApiUrl).then(function (result) {
                // success
                $scope.notes = result.data;
            }, function () {
                // error
                // todo
            });
            
            var socket = io.connect();
            // join a room setup for the current page we are on
            // so we only get updates pertinent to this category
            socket.emit("join category", categoryName);
            // wait for a broadcast note message
            socket.on("broadcast note", function (note) {
                // add the new note someone else added to our page
                $scope.notes.push(note);
                // force data binding to update / show new data
                // we have to force the apply because we aren't in
                // an angular piece of code so it doesn't know what
                // is happening
                $scope.$apply();
            });
            
            $scope.save = function () {
                $http.post(notesApiUrl, $scope.newNote)
                    .then(
                    function (result) {
                        // success
                        $scope.notes.push(result.data);
                        $scope.newNote = createBlankNote();
                        // send the newly created note up to the server, so it
                        // can then be sent to everyone else
                        socket.emit("newNote", { category: categoryName, note: result.data });
                    }, function (err) {
                        // failure
                        // todo
                    }
                );
            };
        }
    ]);
    
    function createBlankNote() {
        return {
            note: "",
            color: "yellow"
        };
    }
})(window.angular);