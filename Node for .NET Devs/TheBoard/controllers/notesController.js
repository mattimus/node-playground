(function (notesController) {
    var data = require("../data");
    var auth = require("../auth");
    
    notesController.init = function (app) {
        app.get("/api/notes/:categoryName", 
            auth.canUseApi,
            function (req, res) {
                var categoryName = req.params.categoryName;
                data.getNotes(categoryName, function (err, category) {
                    if (err) {
                        res.send(400, err);
                    } else {
                        res.set("Content-Type", "application/json");
                        res.send(category.notes);
                    }
                });
            }
        );
        
        app.post("/api/notes/:categoryName", 
            auth.canUseApi,
            function (req, res) {
                var categoryName = req.params.categoryName;
            
                var noteToInsert = {
                    note: req.body.note,
                    color: req.body.color,
                    author: "Shawn Wildermuth"
                };
            
                data.addNote(categoryName, noteToInsert, function (err) {
                    if (err) {
                        res.send(400, "Failed to add note to database.");
                    } else {
                        res.set("Content-Type", "application/json");
                        res.status(201).send(noteToInsert);
                    }
                });
            }
        );
    };
})(module.exports);
