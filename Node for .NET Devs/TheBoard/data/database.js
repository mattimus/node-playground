(function (database) {
    var mongodb = require("mongodb");
    var mongoUrl = "mongodb://localhost:27017/TheBoard"
    var theDb = null;
    
    database.getDb = function (next) {
        if (!theDb) {
            // connect to db
            mongodb.MongoClient.connect(mongoUrl, function (err, db) {
                if (err) {
                    next(err, null);
                } else {
                    // create an object for the db so we can
                    // add things later if neccessary.
                    theDb = {
                        db: db,
                        notes: db.collection("notes"),
                        users: db.collection("users")
                    };
                }
                next(null, theDb);
            })
        } else {
            next(null, theDb);
        }
    }
})(module.exports);
