﻿// updater/index.js
(function (updater) {
    var socketio = require("socket.io");
    
    updater.init = function (server) {
        // start listening
        var io = socketio.listen(server);

        // listen for a connection (this is typically the first message)
        // a connection is opened with each of the clients
        io.sockets.on("connection", function (socket) {
            console.log("A socket was connected");

            socket.on("join category", function(category) {
                socket.join(category);
            });

            socket.on("newNote", function (data) {
                // send this message to members of a particular 
                // category 's room, actually everyone but the 
                // person that we received the message from
                socket.broadcast
                    .to(data.category)
                    .emit("broadcast note", data.note);
            });
        });
    };
})(module.exports);