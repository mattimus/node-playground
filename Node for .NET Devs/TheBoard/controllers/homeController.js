﻿
(function (homeController) {
    var data = require("../data");
    var auth = require("../auth");
    
    homeController.init = function (app) {
        app.get("/", function (req, res) {
            data.getNoteCategories(function (err, results) {
                res.render("index", {
                    title: "The Board",
                    error: err,
                    categories: results,
                    newCatError: req.flash("newCatName"), // this pops the error text out of the flash store
                    user: req.user
                });
            });
        });
        
        // protected for only logged in users
        // it will run through ensure authenticated, and if 
        // you're authenticated will then go (via next)
        // to the second function which renders the page
        app.get("/notes/:categoryName", 
            auth.canViewPages,
            function (req, res) {
                var categoryName = req.params.categoryName;
                res.render("notes", { title: categoryName, user: req.user });
            }
        );
        
        app.post("/newCategory", function (req, res) {
            // Get posted data from the req.body
            var categoryName = req.body.categoryName;
            data.createNewCategory(categoryName, function (err) {
                if (err) {
                    // if adding new category fails, show why and send them bakc to root
                    console.log(err);
                    // this will temporarily add the error to a flash store
                    req.flash("newCatName", err);
                    res.redirect("/");
                } else {
                    // if adding new category succeeds, send them to the new category to add new notes
                    res.redirect("/notes/" + categoryName);
                }
            });
        });
    };
})(module.exports);
