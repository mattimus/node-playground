// use exported props
var exportProps = require("./exports/exportProps.js");
console.log(exportProps.first);
console.log(exportProps.second);
console.log(exportProps.third);

// use exported function
var exportFunction = require("./exports/exportFunction.js");
console.log(exportFunction());

// use exported psuedo class
var exportPsuedoClass = require("./exports/exportPsuedoClass.js");
var psuedoClass = new exportPsuedoClass();
console.log(psuedoClass.first);

// only specifying a folder and requiring the index.js
var logger = require("./logger"); // you can import by the directory if you specify a index.js
logger.log("this is from the logger");

// loading an npm module. npm install underscore --save. then use 
// the same name you used to install to require the package in for use
var _ = require("underscore");
