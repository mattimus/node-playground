﻿(function (auth) {
    
    var data = require("../data");
    var hasher = require("./Hasher.js");
    var passport = require("passport");
    var passportStrategy = require("passport-local").Strategy;
    
    function verifyUser(username, password, next) {
        data.getUser(username, function (err, user) {
            if (!err) {
                var testHash = hasher.hash(password, user.salt);
                if (testHash == user.passwordHash) {
                    next(null, user);
                    return;
                }
            }
            
            next(null, false, { message: "Invalid credentials" });
        });
    }

    auth.canViewPages = function(req, res, next) {
        if (req.isAuthenticated()) {
            next(); // if authed, let page stuff continue
        } else {
            res.redirect("/login"); // if not logged in, send them to the login controller
        }
    };

    auth.canUseApi = function(req, res, next) {
        if (req.isAuthenticated()) {
            next(); 
        } else {
            res.send(401, "Not authorized");
        }
    };
    
    auth.init = function (app) {
        // setup passport auth
        passport.use(new passportStrategy(verifyUser));
        // this is how passport will find a user (we specify by the username)
        passport.serializeUser(function (user, next) {
            next(null, user.username);
        });
        passport.deserializeUser(function (key, next) {
            data.getUser(key, function (err, user) {
                if (err) {
                    next(null, false, "Failed to retrieve user");
                } else {
                    next(null, user);
                }
            });
        });
        app.use(passport.initialize());
        app.use(passport.session());
        
        app.get("/login", function (req, res) {
            res.render("login", { title: "Login", message: req.flash("loginError") });
        });
        
        app.post("/login", function (req, res, next) {
            var authFunc = passport.authenticate("local", function (err, user, info) {
                if (err) {
                    next(err);
                } else {
                    req.login(user, function (err) {
                        if (err) {
                            next(err);
                        } else {
                            res.redirect("/");
                        }
                    });
                }
            });
            authFunc(req, res, next);
        });
        
        app.get("/register", function (req, res) {
            res.render("register", { title: "Register for The Board", message: req.flash("registrationError") });
        });
        
        app.post("/register", function (req, res) {
            var salt = hasher.createSalt();
            var user = {
                name: req.body.name,
                email: req.body.email,
                username: req.body.username,
                passwordHash: hasher.hash(req.body.password, salt),
                salt: salt
            };
            
            data.addUser(user, function (err) {
                if (err) {
                    req.flash("registrationError", "Could not save user to database");
                    res.redirect("/register");
                } else {
                    res.redirect("/login");
                }
            });
        });
    };
})(module.exports);